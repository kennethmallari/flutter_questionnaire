//This will give access to material library
import 'package:flutter/material.dart';
//import the answer button custom widget:
import './answer_button.dart';

class BodyQuestions extends StatelessWidget {
    //Data from the app state(app widget) will be shared to BodyQuestions
    final questions;
    final int questionIdx;
    //if we are receiving a function from somewhere else.
    final Function nextQuestion;

    BodyQuestions({
        required this.questions,
        required this.questionIdx,
        required this.nextQuestion
    });

    @override
    Widget build(BuildContext context) {
        Text questionText =  Text(
                questions[questionIdx]['question'].toString(),
                style: TextStyle(
                fontSize :24.0
            )
        );
    
        //Once you find yourself, having to write a logic, where there's a lot of level of nesting. It is better for us to separate it.
        //Nagbabago ba ito?
        List<String> options = questions[questionIdx]['options'] as List<String>;
            // Before Map ['Time Tracking', 'Asset Management', 'Issue Tracking'];
            //After Map [AnswerButton('Time Tracking'), [AnswerButton('Asset Management')],[AnswerButton('IssueTracking')]]
            List<AnswerButton> answerOptions =  options.map((String option){
                return AnswerButton(text: option, nextQuestion: nextQuestion);
            }).toList();

            // var answerOptions =  options.map((String option){
            //     return AnswerButton(text: option, nextQuestion: nextQuestion);
            // });

        return Container(
        
            width: double.infinity,
            padding: EdgeInsets.all(16) ,
            child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                        questionText,
                        //array of widgets kasi siya, so you need to spread it, kasi kung hindi, mag eerror siya.
                        ...answerOptions,
                        Container(
                            margin: EdgeInsets.only(top:30),
                            width: double.infinity,
                            child: ElevatedButton(
                                child: Text('Skip Question'),
                                onPressed: ()=>nextQuestion(null),
                            )
                        )

                    ]
                ) 
        );
        
    }
    
}