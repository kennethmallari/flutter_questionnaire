import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:flutter_questionnaire/main.dart';

void main() {
  testWidgets('Widgets testing, homework', (WidgetTester tester) async {
    await tester.pumpWidget(MaterialApp(home: App()));

    expect(find.text("Asset Management"), findsOneWidget);
    await tester.tap(find.text('Asset Management'));
    await tester.pump();

    expect(find.text("More than 10,000"), findsOneWidget);
    await tester.tap(find.text('More than 10,000'));
    await tester.pump();

    expect(find.text("Europe"), findsOneWidget);
    await tester.tap(find.text('Europe'));
    await tester.pump();

    expect(find.text("6-9 months"), findsOneWidget);
    await tester.tap(find.text('6-9 months'));
    await tester.pump();

    expect(find.text("What is the nature of your business needs?"), findsOneWidget);
    expect(find.text("Asset Management"), findsOneWidget);
    expect(find.text("What is the expected size of the user base?"), findsOneWidget);
    expect(find.text("More than 10,000"), findsOneWidget);
    expect(find.text("In which region would the majority of the user base be?"), findsOneWidget);
    expect(find.text("Europe"), findsOneWidget);
    expect(find.text("What is the expected project duration?"), findsOneWidget);
    expect(find.text("6-9 months"), findsOneWidget);
  });
}